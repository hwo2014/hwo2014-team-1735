{-# LANGUAGE OverloadedStrings #-}

module CarPositionsModel where

import GameInitModel(CarId(..))

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))
import Control.Monad        (liftM)

import Data.List

-- CarLane

data CarLane = CarLane {
  clStartLaneIndex :: Int,
  clEndLaneIndex   :: Int
} deriving (Show)

instance FromJSON CarLane where
  parseJSON (Object v) =
    CarLane <$>
    (v .: "startLaneIndex") <*>
    (v .: "endLaneIndex")

-- PiecePosition

data PiecePosition = PiecePosition {
  ppIndex :: Int,
  ppDist  :: Double,
  ppLane  :: CarLane,
  ppLap   :: Int
} deriving (Show)

instance FromJSON PiecePosition where
  parseJSON (Object v) =
    PiecePosition <$>
    (v .: "pieceIndex")      <*>
    (v .: "inPieceDistance") <*>
    (v .: "lane")            <*>
    (v .: "lap")

-- CarPosition

data CarPosition = CarPosition {
  cpId            :: CarId,
  cpAngle         :: Double,
  cpPiecePosition :: PiecePosition
} deriving (Show)

instance FromJSON CarPosition where
  parseJSON (Object v) =
    CarPosition <$>
    (v .: "id") <*>
    (v .: "angle") <*>
    (v .: "piecePosition")

-- Helpers

findCar :: String -> [CarPosition] -> Maybe CarPosition
findCar teamName positions =
  find nameMatches positions
  where nameMatches carPosition = ciName (cpId carPosition) == teamName
