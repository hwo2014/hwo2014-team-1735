{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module GameInitModel where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

-- Dimension

data Dimension = Dimension {
  diLength :: Double,
  diWidth  :: Double,
  diGFPos  :: Double
} deriving (Show)

instance FromJSON Dimension where
  parseJSON (Object v) =
    Dimension <$>
    (v .: "length") <*>
    (v .: "width") <*>
    (v .: "guideFlagPosition")

-- CarId

data CarId = CarId {
  ciColor :: String,
  ciName  :: String
} deriving (Show)

instance FromJSON CarId where
  parseJSON (Object v) =
    CarId <$>
    (v .: "color") <*>
    (v .: "name")

-- Car

data Car = Car {
  carId  :: CarId,
  carDim :: Dimension
} deriving (Show)

instance FromJSON Car where
  parseJSON (Object v) =
    Car <$>
    (v .: "id") <*>
    (v .: "dimensions")

-- Your Car

data YourCar = YourCar {
  youTeam  :: String,
  youColor :: String
} deriving (Show)

instance FromJSON YourCar where
  parseJSON (Object v) =
    YourCar <$>
    (v .: "name") <*>
    (v .: "color")

-- Lane

data Lane = Lane {
  laDist :: Int,
  laIdx  :: Int
} deriving (Show)

instance FromJSON Lane where
  parseJSON (Object v) =
    Lane <$>
    (v .: "distanceFromCenter") <*>
    (v .: "index")

-- Piece

data Piece = Piece {
  piLength :: Maybe Double,
  piSwitch :: Maybe Bool,
  piRadius :: Maybe Int,
  piAngle  :: Maybe Double,
  piBridge :: Maybe Bool
} deriving (Show)

instance FromJSON Piece where
  parseJSON (Object v) =
    Piece <$>
    (v .:? "length") <*>
    (v .:? "switch") <*>
    (v .:? "radius") <*>
    (v .:? "angle")  <*>
    (v .:? "bridge")

-- StartingPoint

data StartingPoint = StartingPoint {
  spPos   :: Position,
  spAngle :: Double
} deriving (Show)

instance FromJSON StartingPoint where
  parseJSON (Object v) =
    StartingPoint <$>
    (v .: "position") <*>
    (v .: "angle")

-- Position

data Position = Position {
  poX :: Double,
  poY :: Double
} deriving (Show)

instance FromJSON Position where
  parseJSON (Object v) =
    Position <$>
    (v .: "x") <*>
    (v .: "y")

-- Track

data Track = Track {
  trName   :: String,
  trStart  :: StartingPoint,
  trPieces :: [Piece],
  trLanes  :: [Lane]
} deriving (Show)

instance FromJSON Track where
  parseJSON (Object v) =
    Track <$>
    (v .: "name") <*>
    (v .: "startingPoint") <*>
    (v .: "pieces") <*>
    (v .: "lanes")

-- RaceSession

data RaceSession = RaceSession {
  rsLaps          :: Maybe Int,
  rsDurationMs    :: Maybe Int,
  rsMmaxLapTimeMs :: Maybe Int,
  rsQuickRace     :: Maybe Bool
} deriving (Show)

instance FromJSON RaceSession where
  parseJSON (Object v) =
    RaceSession <$>
    (v .:? "laps") <*>
    (v .:? "durationMs") <*>
    (v .:? "maxLapTimeMs") <*>
    (v .:? "quickRace")

-- Race

data Race = Race {
  raTrack   :: Track,
  raCars    :: [Car],
  raSession :: RaceSession
} deriving (Show)

instance FromJSON Race where
  parseJSON (Object v) =
    Race <$>
    (v .: "track") <*>
    (v .: "cars") <*>
    (v .: "raceSession")

-- GameInitData

data GameInitData = GameInitData {
  giRace :: Race
} deriving (Show)

instance FromJSON GameInitData where
  parseJSON (Object v) =
    GameInitData <$>
    (v .: "race")

-- Helpers

players :: GameInitData -> [CarId]
players gameInit =
  map (\car -> carId $ car) $ raCars $ giRace gameInit

piecesOfGame :: GameInitData -> [Piece]
piecesOfGame gameInit =
  trPieces $ raTrack $ giRace gameInit

lanesOfGame :: GameInitData -> [Lane]
lanesOfGame gameInit =
  trLanes $ raTrack $ giRace gameInit

reportGameInit :: GameInitData -> String
reportGameInit gameInit =
  "Players: " ++ (show $ players gameInit) ++ ", Track: " ++ show (length $ piecesOfGame gameInit) ++ " pieces"
