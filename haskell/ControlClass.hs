{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FunctionalDependencies #-}

module ControlClass where

import Control.Monad.State.Class
import Data.Array.IArray
import Data.List (sortBy)
import Data.Maybe (fromMaybe)
import Data.Ord (comparing)

import GameInitModel
import CarPositionsModel
import Messages

data Ctx = Ctx {
        ctxPieces :: Array Int CPiece,	-- the pieces of the track
        ctxLanes  :: Array Int Double,	-- the lanes of the track
        ctxTeam   :: String		-- my team name (to identify my car)
    }

data CPiece = Line !Bool !Double		-- straight line: switch, length
            | Arc  !Bool !Double !Double	-- curve: switch, radius, angle
     deriving Show

-- This is persistent state which will be passed around between game ticks
data PState i s p = PState {
        stCtx    :: Ctx,	-- context
        stPars   :: p,	-- model parameters
        stPrvSt  :: s,	-- previous model state
        stPrvCtl :: i,	-- previous model input
        stEstSt  :: s,	-- estimated model state
        stLogLs  :: [String]	-- log lines
    }

class Model s m | s -> m where
    type Inp s
    type Par s
    control :: Par s -> s -> m (Inp s)		-- given parameters and state, give next input
    predict :: Inp s -> Par s -> s -> m s	-- predict next state based on input, params & crt state
    correct :: Inp s -> s -> s -> Par s -> m (Par s)	-- correct model parameters from input,
                                                -- prev state, current state and params
    -- initial :: GameInitData -> String -> m (PState (Inp s) s (Par s))
    idstate :: Ctx -> [CarPosition] -> s -> m s	-- identify the current state
    -- extract :: s -> m a -> a	-- extract some info from the monad

class Control c s | c -> s where
    controlInit :: c -> GameInitData -> String -> s
    controlStep :: c -> [CarPosition] -> s -> ([ClientMessage], s)
    controlLog  :: c -> s -> IO s
    controlExtr :: s -> c

controlStepGeneric :: (MonadState ps m, ps ~ PState i s p, Model s m, Inp s ~ i, Par s ~ p)
                   => [CarPosition] -> m (Inp s)
controlStepGeneric carPositions = do
    state  <- get
    curst  <- idstate (stCtx state) carPositions (stPrvSt state)
    nxtpar <- correct (stPrvCtl state) (stPrvSt state) curst (stPars state)
    nxtinp <- control nxtpar curst
    nxtst  <- predict nxtinp nxtpar curst
    modify $ \s -> s { stPars = nxtpar, stPrvSt = curst, stPrvCtl = nxtinp, stEstSt = nxtst }
    return nxtinp

initCtx :: GameInitData -> String -> Ctx
initCtx gi team = Ctx { ctxPieces = pieces, ctxLanes = lanes, ctxTeam = team }
    where ps = piecesOfGame gi
          pieces = listArray (0, length ps - 1) $ map toCPiece ps
          ls = lanesOfGame gi
          lanes = listArray (0, length ls - 1) $ map toLane $ sortBy (comparing laIdx) ls
          toCPiece (Piece { piLength = Just l, piSwitch = pisw })
              = Line (fromMaybe False pisw) l
          toCPiece (Piece { piRadius = Just r, piAngle = Just a, piSwitch = pisw })
              = Arc  (fromMaybe False pisw) (fromIntegral r) a
          toCPiece pc = error $ "This piece is incomplete: " ++ show pc
          toLane (Lane { laDist = d }) = fromIntegral d
