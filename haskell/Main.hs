{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess)

import Network(withSocketsDo, connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))

import GameInitModel
import CarPositionsModel
import Messages
import ControlClass
import ControlStatic

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = withSocketsDo $ do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run :: String -> String -> String -> String -> IO ()
run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  joinMe h botname botkey
  team  <- yourCar h
  c <- initAndRun h team myController	-- qualifying
  _ <- initAndRun h team c		-- race
  return ()

-- Take a message from the server, decode it
getMessage h = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success sm -> return sm
        Error s    -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

-- Send messages to the server
sendMessages :: Handle -> [ClientMessage] -> IO ()
sendMessages = mapM_ . hPutStrLn

pingTheServer h = sendMessages h [pingMessage]

wrongMessage :: String -> ServerMessage -> IO a
wrongMessage s sm = fail $ "Expect " ++ s ++ " message, got " ++ show sm

-- Send join, wait for response, and then ping it
joinMe h botname botkey = do
  sendMessages h [joinMessage botname botkey]
  sm <- getMessage h
  case sm of
    Join -> do
      putStrLn "Joined"
      pingTheServer h
    _    -> wrongMessage "join" sm

-- Wait for your car and ping it; return the team name
yourCar h = do
  sm <- getMessage h
  case sm of
    You yourCar -> do
      let team = youTeam yourCar
      putStrLn $ "Your team is " ++ team ++ " (" ++ youColor yourCar ++ ")"
      pingTheServer h
      return team
    _ -> wrongMessage "your car" sm

-- Wait for initialisation and ping it; return initial state
initAndRun :: Control c s => Handle -> String -> c -> IO c
initAndRun h team c = do
  sm <- getMessage h
  case sm of
    GameInit gameInit -> do
      putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
      pingTheServer h
      runTheRace h gameInit team c
    TourEnd -> do
      putStrLn "Received <TournamentEnd>, exit"
      exitSuccess
    _ -> wrongMessage "game init" sm

runTheRace :: Control c s => Handle -> GameInitData -> String -> c -> IO c
runTheRace h gi team c = do
  let s = controlInit c gi team
  runTheRest h c s

runTheRest :: Control c s => Handle -> c -> s -> IO c
runTheRest h c s = do
  sm <- getMessage h
  handleServerMessage h sm c s

handleServerMessage :: Control c s => Handle -> ServerMessage -> c -> s -> IO c
handleServerMessage h serverMessage c statei = do
  eresp <- respond serverMessage c statei
  case eresp of
    Left (responses, stateo) -> do
      respond serverMessage c statei
      statee <- controlLog c stateo
      sendMessages h responses
      runTheRest h c statee
    Right c -> return c

respond :: Control c s => ServerMessage -> c -> s -> IO (Either ([ClientMessage], s) c)
respond message c state = case message of
  CarPositions carPositions -> return $ Left $ controlStep c carPositions state
  GameEnd -> return $ Right $ controlExtr state
  Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return $ Left ([pingMessage], state)
  sm -> do
    putStrLn $ "Server message: " ++ show sm ++ " not treated"
    return $ Left ([pingMessage], state)
