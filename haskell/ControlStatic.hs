{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE PatternGuards #-}

module ControlStatic (
    myController
) where

import Control.Monad.State
import Control.Monad (when)
import Data.Array.IArray
import Data.List (sortBy)
import Data.Function (on)

import CarPositionsModel
import ControlClass
import Messages

data StaticState = StaticState {
        sCtx  :: Ctx,		-- we need the context to make calculations
        sTick :: !Int,		-- game tick
        sOff  :: !Bool,		-- is it off road?
        sIdx  :: !Int,		-- the index of the current piece
        sLen  :: !Double,	-- total length of the current piece
        sDist :: !Double,	-- distance on the current piece
        sLane :: !Int,		-- current lane
        sVelo :: !Double,	-- velocity
        sAcc  :: !Double,	-- acceleration
        sAng  :: !Double,	-- drift
        sAngV :: !Double,	-- drift rate
        sErr  :: !Double	-- sliding velocity estimation error (quadratic)
    }

instance Show StaticState where
    show ss = "Tick: " ++ show (sTick ss) ++ " sOff: " ++ show (sOff ss)
            ++ " sIdx: " ++ show (sIdx ss) ++ " sLen: " ++ show (sLen ss)
            ++ " sDist: " ++ show (sLen ss) ++ " sLane: " ++ show (sLane ss)
            ++ " sVelo: " ++ show (sVelo ss) ++ " sAcc: " ++ show (sAcc ss)
            ++ " sAng: " ++ show (sAng ss) ++ " sAngV: " ++ show (sAngV ss)
            ++ " sErr: " ++ show (sErr ss)

data StaticParams = StaticParams {
        pLearn :: !Bool,	-- not learning in race
        pPowK :: !Double,	-- gas energy coefficient
        pMass :: !Double,	-- car mass
        pAirC :: !Double,	-- air coefficient
        pTMiu :: !Double,	-- tangential miu
        pRMiu :: !Double	-- radial miu
    } deriving Show

type MyPState = PState Double StaticState StaticParams

instance Control StaticParams MyPState where
    controlInit = controlInitStatic
    controlStep = controlStepStatic
    controlLog  = controlLogStatic
    controlExtr = stPars

myController :: StaticParams
myController  = StaticParams {
                    pLearn = True, pMass = 1000, pPowK = 1,
                    pAirC = 0.01, pTMiu = 0.01, pRMiu = 0.05
                }

controlStepStatic :: StaticParams -> [CarPosition] -> MyPState -> ([ClientMessage], MyPState)
controlStepStatic c cp st
    = let (d, s) = runState (controlStepGeneric cp) st
      in ([throttleMessage d], s)

controlLogStatic :: StaticParams -> MyPState -> IO MyPState
controlLogStatic c st = do
    let t = sTick (stPrvSt st)
    when (t `mod` 100 == 0) $ do
           putStrLn $ "Tick " ++ show t
           let p = stPars st
           putStrLn $ "mass = " ++ show (pMass p)
                  ++ " powk = " ++ show (pPowK p)
                  ++ " airc = " ++ show (pAirC p)
                  ++ " tmiu = " ++ show (pTMiu p)
                  ++ " rmiu = " ++ show (pRMiu p)
           putStrLn $ "Prev: " ++ show (stPrvSt st)
           putStrLn $ "Inpu: " ++ show (stPrvCtl st)
           putStrLn $ "Esti: " ++ show (stEstSt st)
           putStrLn "=============================================="
    if null (stLogLs st)
       then return st
       else do
           mapM_ putStrLn $ reverse (stLogLs st)
           putStrLn "=============================================="
           return st { stLogLs = [] }

debug :: Bool
debug = False

logMes :: String -> InCtl ()
logMes mes = when debug $ modify (\s -> s { stLogLs = mes : stLogLs s })

type InCtl = State MyPState

instance Model StaticState InCtl where
    type Inp StaticState = Double
    type Par StaticState = StaticParams
    control = controlStatic
    predict = predictStatic
    correct = correctStatic
    idstate = idstateStatic

-- Gravitaional constant:
g :: Double
g = 9.81

vmaxReduce = 0.8
vMax r miu = sqrt (r * g * miu) * vmaxReduce

gasForSpeed :: StaticParams -> Double -> Double -> Double
gasForSpeed par v0 v1
    = max 0 $ min 1 $ (pAirC par * v2 + g * pMass par * pTMiu par) * v2 / pPowK par
    where v2 = v1 + (v1 - v0)

realRadius r a dlane | a < 0     = r + dlane	-- turns left
                     | otherwise = r - dlane	-- turns right

bigAngle a = abs a > 30
medAngle a = abs a > 5

controlStatic par state
    | sOff state = return 1
    | Arc _ r a <- cpie = do	-- we are in a curve
        let v0w = vMax (realRadius r a dlane) (pRMiu par)	-- 0 ang velocity
            v0  = sVelo state	-- current velocity
            phi0 = sAng state
            w0   = sAngV state
            big  = bigAngle phi0
            med  = medAngle phi0
            act | phi0 * w0 < 0 = do
                    -- when we are here, it means that on this piece the current
                    -- velocity is ok, and this information should be used later!
                    let v1 = let vok = v0 * 1.2 in if vok > v0w then vok else v0w
                    logMes $ "Tick " ++ show (sTick state) ++ ": in curve but stabilizing"
                    logMes $ "v0w  = " ++ show v0w ++ ", v0 = " ++ show v0
                              ++ " want v1 = " ++ show v1
                    logMes $ "phi0 = " ++ show phi0 ++ ", w0 = " ++ show w0
                    return v1
                | big           = do
                    -- when we are here, it means that on this piece the current
                    -- velocity is too big, and this information should be used later!
                    let v1 = 0
                    logMes $ "Tick " ++ show (sTick state) ++ ": in curve, big"
                    logMes $ "v0w  = " ++ show v0w ++ ", v0 = " ++ show v0
                              ++ " want v1 = " ++ show v1
                    logMes $ "phi0 = " ++ show phi0 ++ ", w0 = " ++ show w0
                    return v1
                | med           = return v0w
                | otherwise     = return $ v0w * 1.05
        v1 <- act
        let u = gasForSpeed par v0 v1
        return $ min 1 u
    | Arc _ r a <- npie = do
        let vmx = vMax (realRadius r a dlane) (pRMiu par)
        if sVelo state <= vmx
           then do
               -- logMes $ "Tick " ++ show (sTick state) ++ ": Curve ahead, radius " ++ show r
               -- logMes $ "Max vel = " ++ show vmx ++ ", our vel = " ++ show (sVelo state)
               return $ variate state
           else do
               let sbreak = breaks vmx
               if sbreak >= srest
                  then return 0
                  else return 1	-- $ min 1 $ 10 - 10 * sbreak / srest
    | otherwise  = return $ variate state
    where parr = ctxPieces $ sCtx state
          cpie = parr ! sIdx state
          (_, mb) = bounds parr
          i = (sIdx state + 1) `mod` (mb + 1)
          npie = parr ! i
          dlane = ctxLanes (sCtx state) ! sLane state
          maxa = g * pTMiu par
          srest = sLen state - sDist state
          v0 = sVelo state
          breaks v | v0 <= v = 0
                   | otherwise = let tb = (v0 - v) / maxa
                                 in tb * (v0 + v) / 2

variate :: StaticState -> Double
variate state | sTick state <= 10 = varThr ! sTick state
              | otherwise         = 1

varThr :: Array Int Double
varThr = listArray (0, 10) [ 1.0, 1.0, 0, 1.0, 0.3, 0.7, 0.1, 0.8, 0.2, 0.3, 0.5 ]

nPred = 100

predictStatic inp par state0 = do
    let v0 = sVelo state0	-- initial velocity
        m  = pMass par
        v1 = predictArr nPred m (g * pTMiu par) (pPowK par * inp) (pAirC par / m) v0
        s = sDist state0 + v1	-- new distance (s = v * t)
    when (v1 < 0) $ do	-- negative velocity!
        logMes $ "Negative velocity!!! " ++ show v1
        logMes $ "Parameters for predictArr:"
        logMes $ "  m    = " ++ show m
        logMes $ "  gmiu = " ++ show (g * pTMiu par)
        logMes $ "  ku   = " ++ show (inp * pPowK par)
        logMes $ "  v0   = " ++ show v0
    if s < sLen state0
       then return $ state0 { sDist = s, sVelo = v1 }
       else do
           let parr = ctxPieces $ sCtx state0
               (_, mb) = bounds parr
               i = (sIdx state0 + 1) `mod` (mb + 1)
               pie = parr ! i
               len = calcLength pie (ctxLanes (sCtx state0) ! sLane state0)
           return $ state0 { sIdx = i, sLen = len, sDist = s - sLen state0, sVelo = v1 }

calcLength :: CPiece -> Double -> Double
calcLength (Line _ l)  _     = l
calcLength (Arc _ r a) dlane = pi * (realRadius r a dlane) * abs a / 180

reduceRMiu = 0.9

correctStatic inp statei statef par
    | not (pLearn par) = return par	-- when the pars are fixed
    | sOff statei = return par
    | sOff statef	-- we are off now: too fast in curve!
        = let parr = ctxPieces $ sCtx statef
              cpie = parr ! sIdx statef
          in case cpie of
                 Arc _ r _ -> let v = sVelo statei	-- previous velocity
                                  rmiu' = v * v / (r * g)
                                  rmiu = if rmiu' < pRMiu par then rmiu' else pRMiu par
                              in return par { pRMiu = rmiu * reduceRMiu }
                 _         -> return par
    | otherwise = return $ optParam (sVelo statei) inp statef par

-- Rate of parameter modifications when learning parameter
optRate = 0.95

optParam :: Double -> Double -> StaticState -> StaticParams -> StaticParams
optParam v0 inp statef pari = snd $ head $ sortBy (compare `on` fst) $
        map (\p -> (velError v0 inp p, p)) [
                  pari,
                  pari { pAirC = pAirC pari / optRate },
                  pari { pAirC = pAirC pari * optRate },
                  pari { pTMiu = pTMiu pari / optRate },
                  pari { pTMiu = pTMiu pari * optRate },
                  pari { pPowK = pPowK pari / optRate },
                  pari { pPowK = pPowK pari * optRate }
              ]

velError :: Double -> Double -> StaticParams -> Double
velError v0 inp par = abs (v1 - v0)
    where v1 = predictArr nPred m (g * pTMiu par) (pPowK par * inp) (pAirC par / m) v0
          m  = pMass par

-- For the beginning we just use the newer param set
combinePar :: StaticParams -> StaticParams -> StaticParams
combinePar p1 p2 = p2

controlInitStatic par gi team
    = PState { stCtx = ctx, stPars = par, stPrvSt = sta, stPrvCtl = 0,
               stEstSt = sta, stLogLs = [ "Initial status: first log line" ]
             }
    where ctx = initCtx gi team
          sta = StaticState {
                    sTick = 0, sCtx = ctx, sOff = False, sIdx = 0,
                    sLen = calcLength (ctxPieces ctx ! 0) (ctxLanes ctx ! 0), sDist = 0, sLane = 0,
                    sVelo = 0, sAcc = 0, sAng = 0, sAngV = 0, sErr = 0
                }

-- Forgetting factor for the sliding error:
alpha = 0.99

idstateStatic ctx cps state0 = do
    let mcp = findCar (ctxTeam ctx) cps
    case mcp of
        Nothing -> fail $ "My car not found (" ++ ctxTeam ctx ++ ")"
        Just cp -> do
            s <- get
            let pp = cpPiecePosition cp
                ix = ppIndex pp
                sl = ppDist pp
                s0 = sDist state0
                off = sTick state0 > 0 && sIdx state0 == ix && s0 == sl
                s1 = if sIdx state0 == ix then sl else sLen state0 + sl
                v = s1 - s0	-- time = 1
                lane = clStartLaneIndex (ppLane pp)
                err = v - sVelo (stEstSt s)
                newSt = StaticState {
                    sTick = sTick state0 + 1, sCtx = ctx, sOff = off, sIdx = ix,
                    sLen  = calcLength (ctxPieces ctx ! ix) (ctxLanes ctx ! lane), sDist = sl,
                    sLane = lane, sVelo = v, sAcc = v - sVelo state0, sAng = cpAngle cp,
                    sAngV = cpAngle cp - sAng state0, sErr = sErr state0 * alpha + err * err
                }
            when (v < 0) $ do
                logMes $ "Negative velocity!!! " ++ show v
                logMes $ "From identifier:"
                logMes $ "  sIdx = " ++ show (sIdx state0)
                logMes $ "  ix   = " ++ show ix
                logMes $ "  sl   = " ++ show sl
                logMes $ "  sLen = " ++ show (sLen state0)
                logMes $ "  s1   = " ++ show s1
                logMes $ "  s0   = " ++ show (sDist state0)
                logMes $ "prevpc = " ++ show (ctxPieces ctx ! sIdx state0)
                logMes $ " lane  = " ++ show lane
                logMes $ "dlane  = " ++ show (ctxLanes ctx ! lane)
            -- when (sIdx (stEstSt s) /= ix) $ do	-- we estimated wrong?
            --     logMes "Estimated piece is different:"
            --     logMes $ "Esti: " ++ show (stEstSt s)
            --     logMes $ "Real: " ++ show newSt
            return newSt

predictArr :: Int -> Double -> Double -> Double -> Double -> Double -> Double
predictArr n m gmiu ku airm v0
    | v0 < eps  = sz!n
    | otherwise = sn!n
    where dt  = 1 / nd		-- the infinitesimal time
          kum = ku / m		-- constant for acceleration
          ve  = kum / gmiu	-- velocity in equilibrium
          v1  = sqrt (2 * kum * dt)	-- formula for velocity without lost
          s1  = dt * v1			-- same for space
          az, an, vz, vn, sz, sn :: Array Int Double
          -- Acceleration is considered constant per finite element
          -- Formulas for v0 ~ 0: index 0 (and for velocity also 1) are treated specially
          az = array (0, n) $ (0, 0) : [(i, acc (vz!i)) | i <- [1..n]]
          vz = array (0, n) $ (0, 0) : (1, v1)
                  : [(i, vz!j + az!j * dt) | i <- [2..n], let j=i-1]
          sz = array (0, n) $ (0, 0) : (1, s1)
                  : [(i, sz!j + vz!j * dt + 0.5 * az!j * dt * dt) | i <- [2..n], let j=i-1]
          -----
          an = array (0, n) $ [(i, acc (vn!i)) | i <- [0..n]]
          vn = array (0, n) $ (0, v0) : [(i, vn!j + an!j * dt) | i <- [1..n], let j=i-1]
          sn = array (0, n) $ (0,  0)
                  : [(i, sn!j + vn!j * dt + 0.5 * an!j * dt * dt) | i <- [1..n], let j=i-1]
          acc v | v <  ve   = max 0 $ accfor v
                | otherwise = min 0 $ accfor v
          accfor v = kum / abs v - airm * abs v - gmiu
          nd = fromIntegral n
          eps = 0.000001
