{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Messages where

import System.Environment (getArgs)

import Network(withSocketsDo, connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
-- import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, Value(..), (.:), Result(..))

import GameInitModel
import CarPositionsModel

type ClientMessage = String

joinMessage :: String -> String -> ClientMessage
joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"

throttleMessage :: Double -> ClientMessage
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"

pingMessage :: ClientMessage
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

data ServerMessage
    = Join
    | You YourCar
    | GameInit GameInitData
    | CarPositions [CarPosition]
    | GameStart
    | GameEnd
    | TourEnd
    | Unknown String
    deriving Show

decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
  | msgType == "join"         = Success Join
  | msgType == "yourCar"      = You <$> (fromJSON msgData)
  | msgType == "gameInit"     = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData)
  | msgType == "gameStart"    = Success GameStart
  | msgType == "gameEnd"      = Success GameEnd
  | msgType == "tournamentEnd"= Success TourEnd
  | otherwise                 = Success $ Unknown msgType

instance FromJSON a => FromJSON (String, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
